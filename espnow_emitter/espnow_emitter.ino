#include "AiEsp32RotaryEncoder.h"
#include "Arduino.h"

#define ROTARY_ENCODER_A_PIN 15  //CLK
#define ROTARY_ENCODER_B_PIN 4  //DT
#define ROTARY_ENCODER_BUTTON_PIN 12  //SW
#define ROTARY_ENCODER_VCC_PIN -1

AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN);


// ESPNOW
#include <esp_now.h>
#include <WiFi.h>
 
uint8_t broadcastAddress[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
 
typedef struct led_struct 
{
  int indexled;
  int color;
} led_struct;


led_struct myLeds;




void setup() 
{
  Serial.begin(115200);

  //we must initialize rorary encoder 
  rotaryEncoder.begin();
  rotaryEncoder.setup([]{rotaryEncoder.readEncoder_ISR();});
  //optionally we can set boundaries and if values should cycle or not
  rotaryEncoder.setBoundaries(0, 24 , false); //minValue, maxValue, cycle values (when max go to min and vice versa)
  rotaryEncoder.reset(0);

  myLeds.indexled=0;
  myLeds.color=0;



  // ESPNOW 
  WiFi.mode(WIFI_STA);
 
  if (esp_now_init() != ESP_OK) 
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
   
  // register peer
  esp_now_peer_info_t peerInfo;
    
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
          
  if (esp_now_add_peer(&peerInfo) != ESP_OK)
  {
    Serial.println("Failed to add peer");
    return;
  }

  myLeds.indexled=0;
  myLeds.color=0;

  Serial.println("START !!!");
}

void loop() 
{
  //in loop call your custom function which will process rotary encoder values
  rotary_loop();
 
}






void rotary_loop() 
{
  //first lets handle rotary encoder button click
  if (rotaryEncoder.currentButtonState() == BUT_RELEASED) 
  {
    //we can process it here or call separate function like:
    rotary_onButtonClick();
  }

  //lets see if anything changed
  int16_t encoderDelta = rotaryEncoder.encoderChanged();
  
  //optionally we can ignore whenever there is no change
  if (encoderDelta == 0) return;
  
  //if value is changed compared to our last read
  if (encoderDelta!=0) 
  {
    //now we need current value
    int16_t encoderValue = rotaryEncoder.readEncoder();
    //process new value. Here is simple output.
    Serial.print("Value: ");
    Serial.println(encoderValue);
   
    myLeds.indexled=encoderValue;
    
    sendIndexColor();
  }   
}



void rotary_onButtonClick() 
{
  Serial.println("BUTTON");

  // debounce
  delay(200);

  // change led color
  myLeds.color++;
  myLeds.color%=3;
  Serial.println(myLeds.color);
  sendIndexColor();
}


void sendIndexColor()
{
  esp_err_t result = esp_now_send  
  (
    broadcastAddress, 
    (uint8_t *) &myLeds,
    sizeof(myLeds));
   
  if (result == ESP_OK) 
  {
    Serial.println("Sent with success");
  }
  else 
  {
    Serial.println("Error sending the data");
  }
}
