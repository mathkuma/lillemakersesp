#include "AiEsp32RotaryEncoder.h"
#include "Arduino.h"

#define ROTARY_ENCODER_A_PIN 15  //CLK
#define ROTARY_ENCODER_B_PIN 4  //DT
#define ROTARY_ENCODER_BUTTON_PIN 12  //SW
#define ROTARY_ENCODER_VCC_PIN -1

AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN);


#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 24

#define DATA_PIN 14  // boom v1.1
//#define CLOCK_PIN 13

// Define the array of leds
CRGB leds[NUM_LEDS];

CRGB ledColor;
int indexColor;
int indexLed;


void setup() 
{
  Serial.begin(115200);

  // ENCODER
  //we must initialize rorary encoder 
  rotaryEncoder.begin();
  rotaryEncoder.setup([]{rotaryEncoder.readEncoder_ISR();});
  //optionally we can set boundaries and if values should cycle or not
  rotaryEncoder.setBoundaries(0, NUM_LEDS , false); //minValue, maxValue, cycle values (when max go to min and vice versa)
  rotaryEncoder.reset(0);

  indexLed=0;

  // FASTLED
  //LEDS.addLeds<WS2812,DATA_PIN,RGB>(leds,NUM_LEDS);
  LEDS.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  
  LEDS.setBrightness(50);

  ledColor = CRGB::Blue;
  indexColor = 0;

  // FASTLED ANIMATION
  // blink all leds
  for (int j=0;j<2;j++)
  {
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=ledColor;
    }
    FastLED.show();
    delay(300);
    
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=CRGB::Black;
    }  
    FastLED.show();
    delay(300);
  }

  Serial.print("START !!!");
}


void loop() 
{
  //in loop call your custom function which will process rotary encoder values
  rotary_loop();
  
  // maj led ring
  for(int i=0;i<indexLed;i++)
  {
    leds[i]=ledColor;
  }
  for(int i=indexLed;i<NUM_LEDS;i++)
  {
    leds[i]=CRGB::Black;
  }
  
  FastLED.show();  
}










void rotary_loop() 
{
  //first lets handle rotary encoder button click
  if (rotaryEncoder.currentButtonState() == BUT_RELEASED) 
  {
    //we can process it here or call separate function like:
    rotary_onButtonClick();
  }

  //lets see if anything changed
  int16_t encoderDelta = rotaryEncoder.encoderChanged();
  
  //optionally we can ignore whenever there is no change
  if (encoderDelta == 0) return;
  
  //if value is changed compared to our last read
  if (encoderDelta!=0) 
  {
    //now we need current value
    int16_t encoderValue = rotaryEncoder.readEncoder();
    //process new value. Here is simple output.
    Serial.print("Value: ");
    Serial.println(encoderValue);
    indexLed = encoderValue;
  }   
}



void rotary_onButtonClick() 
{
  Serial.println("BUTTON");

  // debounce
  delay(200);

  // change led color
  indexColor++;
  indexColor%=3;
  Serial.println(indexColor);

  if(indexColor==0)
  {
    ledColor = CRGB::Blue;
  }
  if(indexColor==1)
  {
    ledColor = CRGB::Red;
  }
  if(indexColor==2)
  {
    ledColor = CRGB::Green;
  }  
}
