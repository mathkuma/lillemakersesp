#include <esp_now.h>
#include <WiFi.h>
 
typedef struct led_struct 
{
  int indexled;
  int color;
} led_struct;


#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 24

#define DATA_PIN 14  // boom v1.1
//#define CLOCK_PIN 13

// Define the array of leds
CRGB leds[NUM_LEDS];

CRGB ledColor;
int indexColor;
int indexLed;



void setup() 
{
  Serial.begin(115200);
  
  // FASTLED
  indexLed=0;

  //LEDS.addLeds<WS2812,DATA_PIN,RGB>(leds,NUM_LEDS);
  LEDS.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  LEDS.setBrightness(100);

  ledColor = CRGB::Blue;
  indexColor = 0;


  // ESPNOW
  WiFi.mode(WIFI_STA);
 
  if (esp_now_init() != ESP_OK) 
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
 
  esp_now_register_recv_cb(OnDataRecv);

  Serial.println("START !!!");
}

void loop() 
{
  // maj led ring
  for(int i=0;i<indexLed;i++)
  {
    leds[i]=ledColor;
  }
  for(int i=indexLed;i<NUM_LEDS;i++)
  {
    leds[i]=CRGB::Black;
  }
  
  FastLED.show();  

}






void OnDataRecv(const uint8_t * mac, const uint8_t *data, int len) 
{
  led_struct* myLeds =(led_struct*) data;
  
  indexLed = myLeds->indexled;
  
  int receivedColor = myLeds->color;

  if(receivedColor==0)
  {
    ledColor = CRGB::Blue;
  }
  if(receivedColor==1)
  {
    ledColor = CRGB::Red;
  }
  if(receivedColor==2)
  {
    ledColor = CRGB::Green;
  }
  
}
