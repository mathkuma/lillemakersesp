//
// A simple server implementation showing how to:
//  * serve static messages
//  * read GET and POST parameters
//  * handle missing pages / 404s
//

#include <Arduino.h>
#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>

AsyncWebServer server(80);

const char* ssid = "Mutualab Coworking 2";
const char* password = "maintenantautravail";

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}




// FASTLED
#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 24

#define DATA_PIN 14  // boom v1.1
//#define CLOCK_PIN 13

// Define the array of leds
CRGB leds[NUM_LEDS];

CRGB ledColor;
int indexColor;
int indexLed;





void setup() {

    Serial.begin(115200);


    // FASTLED
  indexLed=0;

  LEDS.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  
  LEDS.setBrightness(50);

  ledColor = CRGB::Blue;
  indexColor = 0;


  // SPIFFS
  SPIFFS.begin();


  // WIFI    
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.printf("WiFi Failed!\n");
        return;
    }

    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Hello, world");
    });
	
	
	// fichier en SPIFFS
	server.serveStatic("/send_range.html", SPIFFS, "/send_range.html");

    

    // Send a GET request to <IP>/get?message=<message>
    server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
        String message;
        if (request->hasParam(PARAM_MESSAGE)) 
        {
            message = request->getParam(PARAM_MESSAGE)->value();
        } else 
        {
            message = "No message sent";
        }
        request->send(200, "text/plain", "Hello, GET: " + message);
    });

    

    // Send a POST request to <IP>/post with a form field message set to <message>
    server.on("/post", HTTP_POST, [](AsyncWebServerRequest *request){
        String message;
        if (request->hasParam(PARAM_MESSAGE, true)) {
            message = request->getParam(PARAM_MESSAGE, true)->value();
        } else {
            message = "No message sent";
        }
        request->send(200, "text/plain", "Hello, POST: " + message);
    });


    // Send a POST request to <IP>/index
    server.on("/change", HTTP_POST, [](AsyncWebServerRequest *request)
	{
        String sIndex;
        if (request->hasParam("index", true)) 
		{
            sIndex = request->getParam("index", true)->value();
            indexLed = sIndex.toInt();
            Serial.println(indexLed);
			
			// color
			if(indexColor==0)
			  {
				ledColor = CRGB::Blue;
			  }
			  if(indexColor==1)
			  {
				ledColor = CRGB::Red;
			  }
			  if(indexColor==2)
			  {
				ledColor = CRGB::Green;
			  }
        }
		else 
		{
            sIndex = "No index sent";
        }
		
		String sColor;
        if (request->hasParam("color", true)) 
		{
            sColor = request->getParam("color", true)->value();
            indexColor = sColor.toInt();
            Serial.println(indexColor);
			
			// color
			if(indexColor==0)
			{
				ledColor = CRGB::Blue;
			}
			if(indexColor==1)
			{
				ledColor = CRGB::Red;
			}
			if(indexColor==2)
			{
				ledColor = CRGB::Green;
			}
        }
		else 
		{
            sColor = "No color sent";
        }
		
		
        request->send(200, "text/plain", "Hello, POST: index= " + sIndex + "  couleur: " + sColor);
    });


    

    

    server.onNotFound(notFound);

    server.begin();



  // FASTLED ANIMATION
  // blink all leds
  for (int j=0;j<2;j++)
  {
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=ledColor;
    }
    FastLED.show();
    delay(300);
    
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=CRGB::Black;
    }  
    FastLED.show();
    delay(300);
  }
  

  
  
  
  Serial.print("START !!!");


  
}

void loop() 
{

  // maj led ring
  for(int i=0;i<indexLed;i++)
  {
    leds[i]=ledColor;
  }
  for(int i=indexLed;i<NUM_LEDS;i++)
  {
    leds[i]=CRGB::Black;
  }
  
  FastLED.show();
}
