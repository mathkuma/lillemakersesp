#include <ESP8266WiFi.h>
#include <espnow.h>

// MAC emetteur
// 2C:F4:32:4A:2F:5E
uint8_t broadcastAddress[] = {0x2C, 0xF4, 0x32, 0x4A, 0x2F, 0x5E};

// MAC recepteur
// 2C:F4:32:4A:04:CE
//uint8_t broadcastAddress[] = {0x2C, 0xF4, 0x32, 0x4A, 0x04, 0xCE};

typedef struct struct_message {
    int dingdong;
    unsigned long timestamp;
} struct_message;

struct_message aEnvoyer;
struct_message aRecu;

bool onSonne = false;


// LED RGB
#include <FastLED.h>  // https://github.com/FastLED/FastLED

#define NB_LEDS 1
#define LED_DATA_PIN D2

// tableau de leds neopixel
CRGB leds[NB_LEDS];

const int ledPin = BUILTIN_LED;

int ledState = 1;

// BUZZER
#define PIN_BUZZER D5


// HEARTBEAT
unsigned long int previousMillisHB;
unsigned long int currentMillisHB;
unsigned long int intervalHB;
 
void setup()
{
  Serial.begin(115200);

  pinMode(ledPin, OUTPUT);

  // set initial state, LED off
  digitalWrite(ledPin, ledState);
  
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) 
  {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  Serial.println("");
  Serial.println(WiFi.macAddress());

  // Set ESP-NOW Role
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  
  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);


  // LED RGB
  // pour utiliser une led ws2811
  FastLED.addLeds<WS2811, LED_DATA_PIN>(leds, NB_LEDS);
  // pour utiliser une led adafruit neopixel
  //FastLED.addLeds<NEOPIXEL, LED_DATA_PIN>(leds, NB_LEDS);
  FastLED.setBrightness(50);

  blinkLeds(3);

  // BUZZER
  analogWrite(PIN_BUZZER, 0);
  pinMode(PIN_BUZZER, OUTPUT);
  digitalWrite(PIN_BUZZER, LOW);

  shortBeep();


  // HEARTBEAT
  previousMillisHB = millis();
  currentMillisHB = previousMillisHB;
  intervalHB = 2000;
}
 
void loop()
{
  if (onSonne)
  {
    onSonne=false;
    blinkLeds(10);
  }
  
  // manage heartbeat
  currentMillisHB = millis();
  if(currentMillisHB - previousMillisHB > intervalHB)
  {
    previousMillisHB = currentMillisHB;
    Serial.println(F("HeartBeat"));    
  }
}


// Callback when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) 
{
  memcpy(&aRecu, incomingData, sizeof(aRecu));
  Serial.print("Bytes received: ");
  Serial.println(len);

  if (aRecu.dingdong==1)
  {
    Serial.println("ON SONNE");
    Serial.println(aRecu.timestamp);
    onSonne=true;
  }
  
}

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus)
{
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0)
  {
    Serial.println("Delivery success");
  }
  else
  {
    Serial.println("Delivery fail");
  }
}


// gestion d'un bouton poussoir
bool appuiSurBouton(int b)
{
  if (digitalRead(b)==false)
  {
    delay(100);
    while(digitalRead(b)==false)
    {
      // debounce delay
      delay(100);
    }
    return true;
  }
  else
  {
    return false;
  }
}

void blinkLeds(int x)
{
  int delayLed = 50;
  for (int i=0;i<x;i++)
  {
    digitalWrite(ledPin, 0);
    allLedOn(CRGB::Green);
    shortBeep();
    delay(delayLed);
    
    digitalWrite(ledPin, 1);
    allLedOff();
    delay(delayLed);
  }
}


void allLedOn(CRGB cCouleur)
{  
  for (int i=0;i<NB_LEDS;i++)
  {
    leds[i] = cCouleur;
  }  
  FastLED.show();
  
  
  //FastLED.showColor(cCouleur);
  //FastLED.show();
}

void allLedOff()
{
  FastLED.clear();
  FastLED.show();
}



  void shortBeep()
  {
    myTone(1000, 50);
  }

void myTone(int f, int d)
{
    
    analogWrite(PIN_BUZZER, f);
    delay(d);
    analogWrite(PIN_BUZZER, 0);
    
    pinMode(PIN_BUZZER, OUTPUT);
    digitalWrite(PIN_BUZZER, LOW);
}  
