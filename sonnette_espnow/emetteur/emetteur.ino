#include <ESP8266WiFi.h>
#include <espnow.h>

// MAC emetteur
// 2C:F4:32:4A:2F:5E

// MAC recepteur
// 2C:F4:32:4A:04:CE
uint8_t broadcastAddress[] = {0x2C, 0xF4, 0x32, 0x4A, 0x04, 0xCE};

typedef struct struct_message {
    int dingdong;
    unsigned long timestamp;
} struct_message;

struct_message aEnvoyer;
struct_message aRecu;


const int buttonPin = D3;
const int ledPin = BUILTIN_LED;

int buttonState = 0;


// HEARTBEAT
unsigned long int previousMillisHB;
unsigned long int currentMillisHB;
unsigned long int intervalHB;
 
void setup()
{
  Serial.begin(115200);

  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);

  // set initial state, LED off
  digitalWrite(ledPin, buttonState);
  
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  Serial.println("");
  Serial.println(WiFi.macAddress());

  // Set ESP-NOW Role
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  
  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);


  // HEARTBEAT
  previousMillisHB = millis();
  currentMillisHB = previousMillisHB;
  intervalHB = 2000;
}
 
void loop()
{
  
  if (appuiSurBouton(buttonPin))
  {
    buttonState=!buttonState;
    digitalWrite(ledPin, buttonState);
    // Send message via ESP-NOW
    aEnvoyer.dingdong=1;
    aEnvoyer.timestamp=millis();
    esp_now_send(broadcastAddress, (uint8_t *) &aEnvoyer, sizeof(aEnvoyer));

  }
  
 
  
  // manage heartbeat
  currentMillisHB = millis();
  if(currentMillisHB - previousMillisHB > intervalHB)
  {
    previousMillisHB = currentMillisHB;
    Serial.println(F("HeartBeat"));    
  }
}


// Callback when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) 
{
  memcpy(&aRecu, incomingData, sizeof(aRecu));
  Serial.print("Bytes received: ");
  Serial.println(len);
}

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus)
{
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0)
  {
    Serial.println("Delivery success");
  }
  else
  {
    Serial.println("Delivery fail");
  }
}


// gestion d'un bouton poussoir
bool appuiSurBouton(int b)
{
  if (digitalRead(b)==false)
  {
    delay(100);
    while(digitalRead(b)==false)
    {
      // debounce delay
      delay(100);
    }
    return true;
  }
  else
  {
    return false;
  }
}
