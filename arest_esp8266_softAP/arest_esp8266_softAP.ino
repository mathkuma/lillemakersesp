/*
 * control pin
 * http://192.168.4.1/digital/1/1
 * http://192.168.4.1/digital/1/0
 * 
 * read A0
 * http://192.168.4.1/analog/A0
 * 
 * get variable pinD7
 * http://192.168.4.1/pinD7
 * 
 * appel fonction chenille
 * http://192.168.4.1/chenille
 */

// Import required libraries
#include <ESP8266WiFi.h>
#include <aREST.h>

// Create aREST instance
aREST rest = aREST();

// WiFi parameters
const char* ssid = "AREST";
const char* password = "tototiti";

// The port to listen for incoming TCP connections
#define LISTEN_PORT           80

// Create an instance of the server
WiFiServer server(LISTEN_PORT);

// Variables to be exposed to the API
int pinD7Value;

// config LED
int ledPin[4] = {D1, D2, D3, D4};


// Declare functions to be exposed to the API
int ledChenille(String command);

// HEARTBEAT
unsigned long int previousMillisHB;
unsigned long int currentMillisHB;
unsigned long int intervalHB;





void setup(void)
{
  // Start Serial
  Serial.begin(115200);

  for (int i=0;i<4;i++)
  {
    pinMode(ledPin[i], OUTPUT);
    digitalWrite(ledPin[i], LOW);
  }

  // pin D7 en pullup
  pinMode(D7, INPUT);
  digitalWrite(D7, HIGH);

  // Init variables and expose them to REST API
  pinD7Value = digitalRead(D7);
  rest.variable("pinD7",&pinD7Value);

  // Function to be exposed
  rest.function("chenille",ledChenille);

  // Give name & ID to the device (ID should be 6 characters long)
  rest.set_id("90");
  rest.set_name("arest-demo");

  // Setup WiFi network
  WiFi.softAP(ssid, password);
  Serial.println("");
  Serial.println("WiFi created");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);



  // HEARTBEAT
  previousMillisHB = millis();
  currentMillisHB = previousMillisHB;
  intervalHB = 3000;


  Serial.println("START !!!");
}

void loop() 
{
  // heartbeat
  currentMillisHB = millis();
  if(currentMillisHB - previousMillisHB > intervalHB)
  {
    previousMillisHB = currentMillisHB;
    Serial.println(".");
  }


  
  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) 
  {
    return;
  }
  while(!client.available())
  {
    delay(1);
  }
  rest.handle(client);

  // read D7
  //pinD7Value = digitalRead(D7);



  

}


int ledChenille(String command)
//int ledChenille()
{
  for(int i=0;i<4;i++)
  {
    digitalWrite(ledPin[i], LOW);
  }
  
  for(int j=0;j<5;j++)
  {
    for(int i=0;i<4;i++)
    {
      digitalWrite(ledPin[i], HIGH);
      delay(200);
      digitalWrite(ledPin[i], LOW);
      delay(200);
    }

    for(int i=3;i>=0;i--)
    {
      digitalWrite(ledPin[i], HIGH);
      delay(200);
      digitalWrite(ledPin[i], LOW);
      delay(200);
    }
  }
  return 1;
}
