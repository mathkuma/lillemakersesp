# lilleMakersESP

codes test ESP8266/ESP32 pour Lille Makers


## library
git clone https://github.com/FastLED/FastLED.git  

git clone https://github.com/igorantolic/ai-esp32-rotary-encoder.git  
git clone https://github.com/marcoschwartz/aREST.git  

git clone https://github.com/me-no-dev/ESPAsyncWebServer.git  
git clone https://github.com/me-no-dev/ESPAsyncTCP.git  
git clone https://github.com/me-no-dev/AsyncTCP.git  

## Exemples
### esp32 + fastled + encoder  
esp32_fastled  

### aRest
https://github.com/marcoschwartz/aREST

arest_esp8266_softAP  
arest_esp8266_softAP_fastled  

###  ESPnow
tuto et explications
https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/

sketch sans server web  
esp32_fastled  

sketches espnow :  
espnow_emitter  
espnow_receiver  



###  ESPAsyncWebServer
https://github.com/me-no-dev/ESPAsyncWebServer


async_fastled  
async_simple_server  

# Tuto intéressants
divers tuto ESP32 ESP8266  
https://randomnerdtutorials.com/  
https://techtutorialsx.com/  

exemple complet de web server  
https://tttapa.github.io/ESP8266/Chap01%20-%20ESP8266.html

espEasy - domotique  
https://www.letscontrolit.com/wiki/index.php/ESPEasy

rest  
https://www.soapui.org/rest-testing/understanding-rest-parameters.html
