/*
 * http://192.168.4.1/control
 * http://192.168.4.1/control?params=ALERTE
 * http://192.168.43.1/control?params=ALERTE
 */


// FASTLED
#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 16

#define DATA_PIN D4

// Define the array of leds
CRGB leds[NUM_LEDS];


// WIFI + AREST
// Import required libraries
#include <ESP8266WiFi.h>
#include <aREST.h>

// Create aREST instance
aREST rest = aREST();

// WiFi parameters
const char* ssid = "VYPER";
const char* password = "tototiti";

// The port to listen for incoming TCP connections
#define LISTEN_PORT           80

// Create an instance of the server
WiFiServer server(LISTEN_PORT);



// PANNEAU
// Declare functions to be exposed to the API
int controlPanneau(String command);

int ledStatus[8] = {0, 0, 0, 0, 0, 0, 0, 0};


String ledText[8] = {
  "OXYGENE",
  "REACTEUR1",
  "ENERGIE",
  "REACTEUR2",
  "BOUCLIER",
  "REACTEUR3",
  "CMDVOL",
  "ALERTE"
};



void setup(void)
{
  // Start Serial
  Serial.begin(115200);

  // Function to be exposed
  rest.function("control",controlPanneau);

  // Give name & ID to the device (ID should be 6 characters long)
  rest.set_id("98");
  rest.set_name("esp8266-vyper");

  // Setup WiFi network
  WiFi.softAP(ssid, password);
  Serial.println("");
  Serial.println("WiFi created");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);

   
  // FASTLED
  LEDS.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  LEDS.setBrightness(150);

  // FASTLED ANIMATION
  // blink all leds
  for (int j=0;j<2;j++)
  {
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=CRGB::Blue;
    }
    FastLED.show();
    delay(300);
    
    for(int i=0;i<NUM_LEDS;i++)
    {
      leds[i]=CRGB::Black;
    }  
    FastLED.show();
    delay(300);
  }

  // maj des led du panneau
  majLedPanneau();

  Serial.println("START !!!");
}

void loop() 
{
  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) 
  {
    return;
  }
  while(!client.available())
  {
    delay(1);
  }
  rest.handle(client);

  // maj des led du panneau
  majLedPanneau();
}






// gestion du changement de statut du panneau
int controlPanneau(String command)
{
  int changeItem = 99;

  for (int i=0;i<8;i++)
  {
    if(ledText[i]==command)
    {
      changeItem=i;
    }
  }

  if(changeItem<8)
  {
    ledStatus[changeItem]=!ledStatus[changeItem];
  }

  Serial.println(command);
  
  return 1;
}



// maj des led du panneau
void majLedPanneau()
{
  for (int i = 0; i < 8; i++)
  {
    // maj led status
    majLed(i, ledStatus[i]);
  }
}

void majLed(int i, int color)
{
  
  // inverser si ligne 2 ou 4
  switch (i)
  {
    case 2:
      i = 3;
      break;
    case 3:
      i = 2;
      break;
    case 6:
      i = 7;
      break;
    case 7:
      i = 6;
      break;
    default:
      // do nothing
      break;
  }


  if (color == 0)
  {
	  leds[2*i]=CRGB::Green;
	  leds[2*i+1]=CRGB::Green;
  }
  else
  {
    leds[2*i]=CRGB::Red;
	  leds[2*i+1]=CRGB::Red;
  }

  FastLED.show();
}
